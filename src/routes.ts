import * as express from "express";
const router = express.Router();
import userController from "./controller/UserController";

router.route("/").get(userController.getAll).post(userController.postadd);

router.route("/search").get(userController.getsearch);
router
  .route("/:id")
  .get(userController.getOne)
  .put(userController.putOne)
  .delete(userController.deleteOne);


export default router;
