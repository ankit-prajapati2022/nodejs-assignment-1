import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity({ name: "nodejsassignmnet1" })
export class User {
  @PrimaryGeneratedColumn()
  id: Number;

  @Column()
  name: String;

  @Column()
  age: Number;

  @Column()
  breed: String;
}
