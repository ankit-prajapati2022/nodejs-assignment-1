import { User } from "../entity/User";
import { Between, DataSource } from "typeorm";
import { AppDataSource } from "../data-source";
import { Response, Request } from "express";
const userRepo = AppDataSource.getRepository(User);

const controller = {
  getAll: async (req: Request, res: Response) => {
    const users = await userRepo.find();
    res.json(users);
  },

  getOne: async (req: Request, res: Response) => {
    const user = await userRepo.findOneBy({ id: req.params.id });
    res.json(user);
  },

  getsearch: async (req: Request, res: Response) => {
    const age_lte: Number = parseInt(req.query.age_lte);
    const age_gte: Number = parseInt(req.query.age_gte);
    const users = await userRepo
      .createQueryBuilder("user")
      .where("user.age BETWEEN :age_lte AND :age_gte", { age_lte, age_gte })
      .getMany();
    res.send(users);
  },
  putOne: async (req: Request, res: Response) => {
    const user = await userRepo.findOneBy({ id: req.params.id });
    user.name = req.body.name;
    user.age = req.body.age;
    user.breed = req.body.breed;
    await userRepo.save(user);
    res.json(user);
  },
  postadd: async (req: Request, res: Response) => {
    const user = new User();
    user.name = req.body.name;
    user.age = req.body.age;
    user.breed = req.body.breed;
    await userRepo.save(user);
    res.json(user);
  },
  deleteOne: async (req: Request, res: Response) => {
    const user = await userRepo.findOneBy({ id: req.params.id });
    await userRepo.remove(user);
    res.json(user);
  },
};

export default controller;
