import { AppDataSource } from "./data-source";
import { User } from "./entity/User";
import * as express from "express";
import userRoute from "./routes";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


AppDataSource.initialize()
  .then(async () => {

    app.use("/", userRoute);
    app.get("*", (req, res) => {
      res.send("404");
    });

    app.listen(3000, () => {
      console.log("Server started on port 3000");
    });
  })
  .catch((error) => console.log(error));
